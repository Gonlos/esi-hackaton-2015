#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

NUM = 1000000

MAX_NUMBER = NUM+1

number_list = [False]*MAX_NUMBER
prime_list = []

for i in range(2, MAX_NUMBER):
    if not number_list[i]:
        prime_list.append(i)

        for j in range(i, MAX_NUMBER, i):
            number_list[j] = True

def descompon( n ):
    mi_numerito = n
    desc = []
    for p in prime_list:
        if mi_numerito == 1: break
        if mi_numerito < p: break
        while True:
            if mi_numerito == 1: break
            if mi_numerito % p != 0: break
            mi_numerito //= p
            desc.append( p )

    if n == 0:
        return [ 0 ]
    if n == 1:
        return [1]
    if len( desc ) == 0:
        return [ 1, n ]
    return desc

fo = open( '07.out', 'w' )

fo.write( '0\n' )

for i in range( NUM+1 ):
    n1 = sum( map( int, str( i ) ) )

    c2 = ''.join( map( str, descompon( i ) ) )
    n2 = sum( map( int, c2 ) )

    if n1 == n2:
        fo.write( '{}\n'.format( i ) )

fo.close()

