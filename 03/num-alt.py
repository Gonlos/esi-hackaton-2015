#!/usr/bin/env python
# -*- coding: utf-8 -*-
from itertools import permutations
import re

f = open( '03.in', 'r' )

for ln in f:
    l = list( re.findall(r"[\w']+", ln) )

    m = None
    for pn in permutations( l, len( l ) ):
        n = int( ''.join( pn ) )
        if m == None or m < n:
            m = n

    print( m )
