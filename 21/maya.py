#!/usr/bin/env python
# -*- coding: utf-8 -*-

f = open( '21.in', 'r' )

glifo = sorted( f.readline().replace( '\n', '' )  )
glifo = ''.join( glifo )
caracteres = str( f.readline().replace( '\n', '' ))

fo = open( '21.out', 'w' )
ll = len( glifo )
for i in range( len( caracteres ) - ll ):
    if ''.join( sorted( caracteres[i: i+ll] ) ) == glifo:
        fo.write( '{}\n'.format( i ) )

fo.close()

