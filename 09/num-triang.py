#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
from itertools import product

NUM = 100

a = 0
triangulares = []
for i in range( NUM+1 ):
    a += i
    triangulares.append( a )


for i in range( 1, NUM+1 ):
    for t, j in zip( triangulares, range( len( triangulares ) ) ):
        indx = j
        if i < t: break

    for t in product( triangulares[:indx], repeat=3 ):
        if sum( t ) == i:
            print( '{} = {}'.format( i, ' + '.join( map(str, t  )  ) ) )
            break

