#!/usr/bin/env python
# -*- coding: utf-8 -*-

def capicua( num ):
    s = str( num )
    l = len( s )
    for i in range( l ):
        if s[i] != s[l-i-1]:
            return False
    return True

NUM_MIN = 1000
NUM_MAX = 2000

for n in range( NUM_MIN, NUM_MAX ):
    if capicua( n ):
        print( n )
