#!/usr/bin/env python
# -*- coding: utf-8 -*-
 

def hamiltonian(edges):
    node2colnums = {} # Maps node to col indices of incident edges.
    for colnum, edge in enumerate(edges):
        n1, n2 = edge
        node2colnums.setdefault(n1, []).append(colnum)
        node2colnums.setdefault(n2, []).append(colnum)

    lp = glpk.LPX()                     # A new empty linear program
    glpk.env.term_on = False            # Stop annoying messages.
    lp.cols.add(len(edges))             # A struct var for each edge
    lp.rows.add(len(node2colnums)+1)    # Constraint for each node

    for col in lp.cols:                 # Go over all struct variables
        col.kind = bool                 # Make binary, not continuous

    # For each node, select at least 1 and at most 2 incident edges.
    for row, edge_column_nums in zip(lp.rows, node2colnums.values()):
        row.matrix = [(cn, 1.0) for cn in edge_column_nums]
        row.bounds = 1, 2

    # We should select exactly (number of nodes - 1) edges total
    lp.rows[-1].matrix = [1.0]*len(lp.cols)
    lp.rows[-1].bounds = len(node2colnums)-1

    assert lp.simplex() == None         # Should not fail this way.
    if lp.status != 'opt': return None  # If no relaxed sol., no exact sol.

    assert lp.integer() == None         # Should not fail this way.
    if lp.status != 'opt': return None  # Count not find integer solution!

    # Return the edges whose associated struct var has value 1.
    return [edge for edge, col in zip(edges, lp.cols) if col.value > 0.99]

# class V(object):
    # def __init__(self, n, *nl):
        # self.i = n
        # self.nl = list(nl)
 
    # def __hash__(self):
        # return self.i
 
    # def contiene(self, v):
        # if isinstance(v, str):
            # return v in self.nl
        # return self.contiene(v.i)
 
# class G(object):
    # def __init__(self):
        # self.visitados = {}
 
    # def add(self, n, *nl):
        # self.visitados[n] = V(n, *nl)
 
    # def hamiltoniano(self, current = None, pending = None, destiny = None):
        # if pending is None:
            # pending = self.visitados.values()
 
        # result = None
 
        # if current is None:
            # for current in pending:
                # result = self.hamiltoniano(current, [x for x in pending if x is not current], current)
                # if result is not None:
                    # break
        # else:
            # if pending == []: 
                # if current.contiene(destiny):
                    # return [current]
                # else:
                    # return None
 
            # for x in [self.visitados[v] for v in current.nl]:
                # if x in pending:
                    # result = self.hamiltoniano(x, [y for y in pending if y is not x], destiny)
                    # if result is not None:
                        # result = [current] + result
                        # break    
 
        # return result

f = open( '23-p.in', 'r' )

f.readline()

nodos = []

for l in f:
    l = l.replace( '\n', '' )

    if l == '-Aristas-':
        break

    nodos.append( l )

aristas = []
for l in f:
    l = l.replace( '\n', '' )
    a, b = l.split( ':' )
    aristas.append( (a, b) )

lh = hamiltonian( aristas )

print( lh )

# ss = set()
# for v in lh:
    # ss.add( v.i )

# s = set()
# for v in lh:
    # for v2 in v.nl:
        # if not v2 in ss: continue
        # t = tuple( sorted( [v.i, v2] ) )
        # s.add( t )

# for a, b in sorted( s ):
    # print( '{}:{}'.format( a, b ) )
