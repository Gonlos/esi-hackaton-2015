#!/usr/bin/python
import random
import math
import random
import multiprocessing
from itertools import combinations, groupby

NUM_E = 2.71828182846

class Nodo(object):
    def get_peso(self):
        raise NotImplementedError

    def get_lista_sucesores(self):
        raise NotImplementedError

class TempleSimulado(object):
    def __init__(self, estado_inicial, num_max_iteraciones = 100000):
        self._estado_actual = estado_inicial
        self._num_max_iteraciones = num_max_iteraciones
        self._iteracion = 0
        self._probabilidad = 1.0

    def actualizar_probabilidad(self):
        raise NotImplementedError

    def calcular( self, num_iteraciones = -1 ):
        l = max( num_iteraciones, self._num_max_iteraciones )

        peso_actual = self._estado_actual.get_peso()
        while self._probabilidad > 1e-30:
            print( 'Iteracion {}: Coste {}'.format( self._iteracion, peso_actual ) )

            lista_sucesores = self._estado_actual.get_lista_sucesores()

            try:
                nuevo_estado = random.choice( lista_sucesores )
            except:
                return self._estado_actual

            nuevo_peso = nuevo_estado.get_peso()
            diferencia_peso = nuevo_peso - peso_actual
            if diferencia_peso > 0:
                self._estado_actual = nuevo_estado
                peso_actual = nuevo_peso
            else:
                a = random.random()
                b = NUM_E ** ( diferencia_peso / float( self._probabilidad ) )
                if a < b:
                    self._estado_actual = nuevo_estado
                    peso_actual = nuevo_peso

            self.actualizar_probabilidad()
            self._iteracion += 1

            if self._iteracion > l:
                return self._estado_actual

        return self._estado_actual



def _make_temple_simulado(estado_inicial, num_max_iteraciones ):
    return TempleSimuladoLineal( estado_inicial, num_max_iteraciones )

class TempleSimuladoMultithread(object):
    def __init__(self, estado_inicial, num_max_iteraciones = 100000, num_hilos=5):
        self._pool = multiprocessing.Pool(processes = num_hilos )
        self._estado_inicial = estado_inicial
        self._num_max_iteraciones = num_max_iteraciones
        self._num_hilos = num_hilos

    def calcular(self):
        ll = self._pool.map( _calcular_tread, map( lambda x: ( self._estado_inicial, self._num_max_iteraciones ),
            range(self._num_hilos) ) )
        
        return reduce( lambda x, y: x if x.get_peso() > y.get_peso() else y, ll )

class TempleSimuladoLineal(TempleSimulado):
    def actualizar_probabilidad(self):
        self._probabilidad -= 0.000005

class NodoSudoku(Nodo):
    def __init__(self, valores):
        self._peso = None
        self.sudoku = valores

    def get_peso(self):
        if self._peso == None:
            self._peso = self._get_peso()
        return self._peso

    def _get_peso(self):
        num_fallos = 0
        for i in range( 9 ):
            for j, k in combinations( range( 9 ), 2 ):
                if self.sudoku[ i*3 + j ] == None or self.sudoku[ i*3 + k ] == None : continue
                if self.sudoku[ i*3 + j ] == self.sudoku[ i*3 + k ]:
                    num_fallos += 1

        dd = []
        dd.append([[],[],[]])
        dd.append([[],[],[]])
        dd.append([[],[],[]])

        for i in range( 9 ):
            for j in range( 9 ):
                if self.sudoku[ i*3 + j ] == None: continue
                dd[ i // 3 ][ j // 3 ].append( self.sudoku[ i*3 + j ] )

        for a in dd:
            for b in a:
                for _, l in groupby( sorted( b ) ):
                    num_fallos += len( list( l ) ) - 1

        return -num_fallos

    def get_lista_sucesores(self):
        if self.get_peso() == 0:
            if len( list(filter( lambda x: x == None, self.sudoku ) ) ) == 0:
                return []

        sucesores = [ self ]
        for c in range(20):
            b = random.randint( 0, 8 )

            for i in range( 0, 9*9 ):
                if self.sudoku[ i ] == None:
                    s = self.sudoku[:]
                    s[i] = b
                    sucesores.append( NodoSudoku( s ) )
                    break

        return sucesores

s = '004800017670900000508030004300740100069000780001069005100080306000006091240001500'
a = list( map( lambda x: int(x) if x != '0' else None, s ) )

n = NodoSudoku( a )
#ts = TempleSimuladoMultithread( n )
ts = TempleSimuladoLineal( n )
sol = ts.calcular()
print( sol.get_peso() )
print(sol.sudoku)
