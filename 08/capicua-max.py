#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

MIN_NUM_DIGITOS = 2
MAX_NUM_DIGITOS = 9

# for i in range(2, MAX_NUMBER):
    # if not number_list[i]:
        # prime_list.append(i)

        # for j in range(i, MAX_NUMBER, i):
            # number_list[j] = True

def capicua( num ):
    s = str( num )
    l = len( s )
    for i in range( l ):
        if s[i] != s[l-i-1]:
            return False
    return True

lr = []
for n in range( MIN_NUM_DIGITOS, MAX_NUM_DIGITOS+1 ):
    n1 = 10**(n*2-1)-1
    n2 = int( '9' * ( n*2 ) )

    for i in range( n2, n1, -1 ):
        if not capicua( i ): continue

        if descompon_2( i ):
            print( i )
            lr.append( i )
            break

print( lr )
