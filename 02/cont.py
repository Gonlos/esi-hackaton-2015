#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from itertools import groupby

f = open( '02.in', 'r' )
texto = f.readlines()

texto = ' '.join( texto )

l = []
for w, lst in groupby( sorted( map( lambda x: x.lower(), re.findall(r"[\w']+", texto) ) ) ):
    l.append( ( len( list( lst ) ), w ) )

for a, b in reversed( sorted( l ) ):
    print( b, a )


