%% Numeritos mios

minumerito :- numerito( 14, L ).

numerito( 0, [] ) :- !.
numerito( N, _ ) :- N < 0, fail.

numerito( N, [' + 5'| L] ) :- NN is N - 5, write( ' + 5' ), numerito( NN, L ).
numerito( N, [' * 3'| L] ) :- R is N mod 3, R =:= 0, NN is N // 3, write( ' * 3' ), numerito( NN, L ).
