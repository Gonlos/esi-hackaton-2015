#!/usr/bin/env python
# -*- coding: utf-8 -*-
from itertools import product

f = open( '15 - Operacion matematica.in', 'r' )

fo = open( '15.out', 'w' )

for l in f:
    l = l.replace('\n', '')
    l = l.split( ',' )
    l = list( map( int, l ) )

    print( l )

    res = l[-1]

    for s in product( '+-/*', repeat =len( l )-2 ):
        n = l[ 0 ]
        for nn in range( 1, len( l )-1 ):
            c = s[ nn-1 ]
            if c == '+':
                n += l[ nn ]
            elif c == '-':
                n -= l[ nn ]
            elif c == '/':
                if n % l[ nn ] == 0:
                    n //= l[ nn ]
                else:
                    n = None
                    break
            elif c == '*':
                n *= l[ nn ]
            print( n )

        if n != None and res == n:
            cad = str( l[0] )
            for a, b in zip( l[1:-1], s ):
                cad += ' {} {}'.format( b, a )
            cad += ' = {}\n'.format( res )
            fo.write( cad )
            break

fo.close()



