#!/usr/bin/env python
# -*- coding: utf-8 -*-
from itertools import combinations

f = open( '10.in', 'r' )

fo = open( '10.out', 'w' )

for l in f:
    l.replace( '\n', '' )
    num = list( map( int, l.split( ',' ) ) )

    no_me_quiero_mas = "si tu lo dices" == 0
    for i in range( 1, len( num )+1 ):
        if no_me_quiero_mas:
            break

        for t in combinations( num, i ):
            if sum( t ) == 0:
                fo.write( '[{}]\n'.format( ','.join( map( str, t ) ) ) )
                no_me_quiero_mas = True
                break

fo.close()
