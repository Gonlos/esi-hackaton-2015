#!/usr/bin/env python
f = open( '13.in', 'r', encoding = "ISO-8859-1")

d = {}
for l in f:
    l = l.replace( '\n', '' )
    l.upper()
    d[ ''.join(sorted( l )) ] = []

f = open( '13.in', 'r', encoding = "ISO-8859-1")
for l in f:
    l = l.replace( '\n', '' )
    l.upper()
    g = ''.join(sorted( l ))
    d[ g ].append( l )

l = []
for _, ll in d.items():
    if len( ll ) < 2: continue
    l.append( sorted( ll ) )

fo = open( '13.out', 'w' )
for ll in sorted( l ):
    fo.write( '{}\n'.format( '  '.join( ll ) ) )

fo.close()

