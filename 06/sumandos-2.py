def sumandos( s, s2, a, n, nivel, m, num ):
    if nivel > m: return
    for i in range( 1, n+1 ):
        b = list( a )
        b.append( i )
        cad = ','.join(map( str, sorted(b) ) )

        if cad in s2: return

        if sum( b ) == num:
            s2.add( cad )
            s.add( cad )
        sumandos( s, s2, b, n-i, nivel+1, m, num )

NUM = 2542
M = 10

s = set()
sumandos( s, set(), [], NUM, 1, M, NUM )

l = map( lambda x: x.split(','), s )

l = reversed( sorted( map( lambda x: ( -len( x ), list( reversed( sorted( map( int, x ) ) ) ) ), l ) ) )

for _, nums in l:
    print( '+'.join( map( str, nums ) ) )
