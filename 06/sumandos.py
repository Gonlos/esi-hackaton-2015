def sumandos( s, a, n, nivel, m, num ):
    if nivel > m: return
    for i in range( 1, n+1 ):
        b = a[:]
        b.append( i )
            
        cad = ','.join(map( str, sorted(b) ) )

        if cad in s: return

        if n-i == 0:
            s.add( cad )
        sumandos( s, b, n-i, nivel+1, m, num )
        
NUM = 2542
M = 4

s = set()
sumandos( s, [], NUM, 1, M, NUM )

l = map( lambda x: x.split(','), s )

l = reversed( sorted( map( lambda x: ( -len( x ), list( reversed( sorted( map( int, x ) ) ) ) ), l ) ) )

for _, nums in l:
    print( '+'.join( map( str, nums ) ) )
