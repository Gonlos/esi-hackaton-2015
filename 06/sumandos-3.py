from itertools import product

NUM = 100
M = 4

s = set()
for x in product( range( NUM+1 ), repeat = M ):
    cad = '+'.join( map( str, sorted(filter( lambda x: x != 0, x ) ) ) )
    if cad != '':
        s.add( cad )

l = filter( lambda x: sum( map( int, x.split( '+' ) ) ) == NUM, s )

# s = set()
# for x in l:
    # cad = '+'.join( map( str, sorted(filter( lambda x: x != 0, x ) ) ) )
    # s.add( cad )

for cad in l:
    print( cad )
