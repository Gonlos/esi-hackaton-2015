#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import math

f = open( '05.in', 'r' )

clavos = []
for l in f:
    a = map( int, re.findall(r"[\w']+", l) )
    clavos.append( tuple( a ) )

clavos.sort()

clavo_inicial = clavos[0]
clavos_goma = [ clavo_inicial ]

angulo = 95
ca = clavo_inicial
while True:
    ctmp = ( None, -1, None )
    for c in clavos:
        if c == ca: continue

        px = c[0]-ca[0]
        py = c[1]-ca[1]
        if px != 0:
            a = math.degrees( math.atan( py/px ) )
        else:
            a = 90 if py >= 0 else 270

        if px < 0 and py >= 0:
            a += 180
        elif px < 0 and py < 0:
            a += 180
        elif px >= 0 and py < 0:
            a  += 360

        a2 = ( a - angulo ) % 360

        if ctmp[1] < a2:
            ctmp = ( c, a2, a )

    ca = ctmp[0]
    
    if ca == clavo_inicial:
        break

    angulo = ( ctmp[ 2 ] + 5 ) % 360
    clavos_goma.append( ca )

for x, y in sorted( clavos_goma ):
    print( '{},{}'.format( x, y ) )

